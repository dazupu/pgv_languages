<?php
/**
 * Chinese Language file for PhpGedView.
 *
 * phpGedView: Genealogy Viewer
 * Copyright (C) 2002 to 2008  PGV Development Team.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package PhpGedView
 * @version $Id: configure_help.zh.php 4194 2008-10-30 10:45:46Z fisharebest $
 * @translation mr_bobwang
 */

if (!defined('PGV_PHPGEDVIEW')) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

//-- CONFIGURE FILE MESSAGES
$pgv_lang["configure"]            = "處理配置 PhpGedView";
//-- edit privacy messages
$pgv_lang["edit_privacy"]        = "編輯私隱性";
$pgv_lang["SHOW_LIVING_NAMES"]    = "顯示在世人士名單";
//-- language edit utility
$pgv_lang["edit_langdiff"]        = "編輯語言檔";
$pgv_lang["edit_lang_utility"]        = "語言編輯工具";
$pgv_lang["edit_lang_utility_help"]    = "您能使用這項公共事業編輯語言檔的內容由使用內容英國一個。<br />它將列出您原始的英語檔的內容和您選上的語言內容<br />在點擊在您選上的檔消息以後一個新窗口將打開您能改變和保存您選上的語言消息的地方。";
$pgv_lang["language_to_edit"]        = "編輯用的語言";
$pgv_lang["file_to_edit"]        = "用於編輯的檔案";
$pgv_lang["check"]            = "檢查";
$pgv_lang["lang_save"]            = "保存語言";
$pgv_lang["contents"]            = "內容";
$pgv_lang["listing"]            = "目錄";
$pgv_lang["no_content"]        = "沒有內容";
$pgv_lang["editlang"]            = "編輯";
$pgv_lang["editlang_help"]        = "~#pgv_lang[editlang]#~<br /><br />編輯語言檔的資訊<br />";
$pgv_lang["savelang"]            = "保存語言";
$pgv_lang["cancel"]            = "取消";
$pgv_lang["savelang_help"]        = "#pgv_lang[savelang]#~<br /><br />保存資訊到語言檔。<br />";
$pgv_lang["original_message"]        = "原有信息";
$pgv_lang["message_to_edit"]        = "編輯信息";
$pgv_lang["changed_message"]        = "變更後的信息";
$pgv_lang["message_empty_warning"]    = "-> 警告！此信息不存在 #LANGUAGE_FILE# <- 內。";
$pgv_lang["language_to_export"]    = "輸出用的語言";
$pgv_lang["export_lang_utility"]    = "輸出語言的工具";
$pgv_lang["export"]            = "輸出";
$pgv_lang["compare_lang_utility"]    = "語言比較的工具";
$pgv_lang["new_language"]        = "新的語言";
$pgv_lang["compare"]            = "比較";
$pgv_lang["comparing"]        = "比較的文件";
$pgv_lang["config_lang_utility"]                  = "設置語言的工具";
$pgv_lang["active"]            = "工作狀態中";
$pgv_lang["edit_settings"]        = "編輯設置";
$pgv_lang["lang_edit"]            = "語言編輯";
$pgv_lang["lang_language"]        = "語言-語種";
$pgv_lang["export_filename"]        = "輸出檔案名：";
$pgv_lang["lang_name_chinese"]    = "語言名稱-中文";
$pgv_lang["lang_name_english"]    = "語言名稱-英文";
$pgv_lang["lang_new_language"]    = "新的語言";
$pgv_lang["lang_langcode"]        = "語言的網頁代碼號";
$pgv_lang["lang_filenames"]        = "語言檔";
$pgv_lang["flagsfile"]            = "標誌文件";
$pgv_lang["text_direction"]        = "文本方向";
$pgv_lang["date_format"]        = "日期格式 (年月日的排列)";
$pgv_lang["time_format"]        = "時間格式";
$pgv_lang["week_start"]        = "一周開始日";
$pgv_lang["name_reverse"]        = "姓氏在名字前(與歐式相反)";
$pgv_lang["ltr"]                          = "從左到右";
$pgv_lang["rtl"]                  =  "從右到左";
$pgv_lang["file_does_not_exist"]    = "錯誤！文件不存在...";
$pgv_lang["optional_file_not_exist"]    = "多選的文件不存在。";
$pgv_lang["multi_letter_alphabet"]    = "多字母的單字";
$pgv_lang["dictionary_sort"]        = "按英文字母順序排列";
$pgv_lang["add_new_lang_button"]    = "增加新語言按鍵";
$pgv_lang["hide_translated"]        = "不要顯示翻譯資訊";
$pgv_lang["users_langs"]        = "使用者的語言";
$pgv_lang["configured_languages"]    = "語言配置";
$pgv_lang["um_header"]         = "用戶轉移工具";
$pgv_lang["um_creating"]         = "建立中";
$pgv_lang["um_file_create_fail2"]     = "不可建立";
$pgv_lang["um_import"]         = "讀入";
$pgv_lang["um_export"]         = "輸出";
$pgv_lang["um_imp_succ"]         = "讀入成功";
$pgv_lang["um_imp_fail"]         = "讀入失敗";
$pgv_lang["um_backup"]         = "備份";
$pgv_lang["um_zip_succ"]         = "檔案壓縮zip成功";
$pgv_lang["um_zip_dl"]             = "下載壓縮的備份檔案";
$pgv_lang["um_bu_config"]         = "家譜設置檔";
$pgv_lang["um_bu_gedcoms"]         = "家譜文件";
$pgv_lang["um_bu_media"]        = "多媒體檔案";
$pgv_lang["um_mk_bu"]         = "創建悲憤bu?";
$pgv_lang["um_results"]        = "結果";
$pgv_lang["ged_filter_results"]         = "搜索結果";
$pgv_lang["ged_filter_reset"]         = "清除搜索";
$pgv_lang["ged_filter_description"]     = "搜索選項";
$pgv_lang["new_gedcom_title"]                 = "家譜來自於 [#GEDCOMFILE#] 文件";
$pgv_lang["USE_MEDIA_VIEWER"]                  = "使用媒體瀏覽器";
$pgv_lang["SHOW_PRIVATE_RELATIONSHIPS"]    = "顯示個人關係";
$pgv_lang["AUTO_GENERATE_THUMBS"]    = "自動建立縮圖";
$pgv_lang["SHOW_SOURCES"]            = "顯示來源";
$pgv_lang["UNDERLINE_NAME_QUOTES"]    = "名字加上底線";
$pgv_lang["gedcom_conf"]                      = "家譜簡單資訊";
$pgv_lang["media_conf"]            = "多媒體";
$pgv_lang["media_general_conf"]                  = "普通";
$pgv_lang["displ_names_conf"]                                  = "名字";
$pgv_lang["displ_layout_conf"]                                   = "格式";
$pgv_lang["contact_conf"]                         = "聯繫方法";
$pgv_lang["DBUSER"]                              = "資料庫用戶名";
$pgv_lang["DBNAME"]                              = "資料庫名稱";
$pgv_lang["upload_path"]                = "上載資料夾";
$pgv_lang["LANGUAGE"]                = "語言";
$pgv_lang["ENABLE_MULTI_LANGUAGE"]        = "允許使用者改變語言";
$pgv_lang["CALENDAR_FORMAT"]            = "日曆格式";
$pgv_lang["DEFAULT_PEDIGREE_GENERATIONS"]  = "幾代人";
$pgv_lang["MAX_DESCENDANCY_GENERATIONS"] = "最多展示幾代人？";
$pgv_lang["GENERATE_GUID"]                           = "自動建立世代編碼";
$pgv_lang["GEDCOM_ID_PREFIX"]             = "個人代號編碼";
$pgv_lang["SOURCE_ID_PREFIX"]             = "來源的代號編碼";
$pgv_lang["REPO_ID_PREFIX"]                           = "資料夾的代號編碼";
$pgv_lang["PEDIGREE_FULL_DETAILS"]                       = "在譜系圖表中顯示出生和去世日期";
$pgv_lang["PEDIGREE_SHOW_GENDER"]            = "在譜系圖表中顯示性別標誌";
$pgv_lang["ZOOM_BOXES"]                     = "圖表中有放大功能";
$pgv_lang["SHOW_AGE_DIFF"]                     = "顯示年齡差距";
$pgv_lang["HIDE_LIVE_PEOPLE"]                 = "隱藏在世人士";
$pgv_lang["REQUIRE_AUTHENTICATION"]             = "需要驗証";
$pgv_lang["WELCOME_TEXT_AUTH_MODE"]  = "登入頁有歡迎短信";
$pgv_lang["WELCOME_TEXT_AUTH_MODE_CUST"]= "自己的歡迎短信";
$pgv_lang["ALLOW_EDIT_GEDCOM"]         = "允許在網路上編輯";
$pgv_lang["EDIT_AUTOCLOSE"]          = "自動關閉編輯視窗";
$pgv_lang["POSTAL_CODE"]  = "郵編號碼";
$pgv_lang["SUBLIST_TRIGGER_I"]         = "最多的姓氏";
$pgv_lang["SUBLIST_TRIGGER_F"]         = "最多的姓氏";
$pgv_lang["SURNAME_LIST_STYLE"]         = "姓氏列表的格式";
$pgv_lang["NAME_FROM_GEDCOM"]         = "從家譜文件中讀取姓名";
$pgv_lang["SHOW_LAST_CHANGE"]                 = "顯示最後更改家譜記錄的日期";
?>