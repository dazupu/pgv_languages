<?php
/**
 * Chinese extra definitions file for PhpGedView.
 *
 * PhpGedView: Genealogy Viewer
 * Copyright (C) 2002 to 2011  PGV Development Team.  All rights reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package PhpGedView
 * @author Huang Peter B.C.
 * @created 2011-09-30
 * @version $Id$
 */

if (!defined('PGV_PHPGEDVIEW')) {
	header('HTTP/1.0 403 Forbidden');
	exit;
}
//-- Define Chinese extra definitions
$pgv_lang["chinese_chart"]                                      = "吊線圖表";
$pgv_lang["data_entry_instruction_1"]                 = "建譜資料輸入：在「個人選項」功能表上選擇適當的編輯選項。";
$pgv_lang["data_entry_instruction_2"]                 = "注意: 日期輸入格式 - 日 月 年 (範例: 1 3 1900)";
$pgv_lang["NICK3"]   = "字";
$pgv_lang["NICK2"]   = "號";
$pgv_lang["NICK1"]                                                     = "別名 ";
$pgv_lang["lunarcalendar"]                                       = "農曆";
$pgv_lang["show_spouses"]                                      = "顯示配偶";
$pgv_lang["chinesechronology"]                              = "皇歷-西元對照表";
$pgv_lang["all_dynasty"]                                            = "歷代皇朝列表";
$pgv_lang["tang_dynasty"]                                        = " 唐朝 ";
$pgv_lang["five_dynasty"]                                          = " 五代 ";
$pgv_lang["ten_kingdoms_dynasty"]                       = " 十國 ";
$pgv_lang["song_dynasty"]                                        = " 宋朝 ";
$pgv_lang["northern_song_dynasty"]                      = " 北宋 ";
$pgv_lang["southern_song_dynasty"]                      = " 南宋 ";
$pgv_lang["liao_dynasty"]                                           = " 遼 ";
$pgv_lang["jin_dynasty"]                                             = " 金 ";
$pgv_lang["yuan_dynasty"]                                         = " 元朝 ";
$pgv_lang["ming_dynasty"]                                         = " 明朝 ";
$pgv_lang["qing_dynasty"]                                           = " 清朝 ";
$pgv_lang["republic_china"]                                        = " 民國 ";
?>