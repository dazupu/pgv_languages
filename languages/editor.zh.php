<?php
/**
 * Chinese texts
 *
 * phpGedView: Genealogy Viewer
 * Copyright (C) 2002 to 2011  PGV Development Team.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * @author PGV Developers
 * @package PhpGedView
 * @subpackage Languages
 * @version $Id: editor.zh.php 7036 2011-04-02 19:59:17Z canajun2eh $
 * @translation mr_bobwang
 */

if (!defined('PGV_PHPGEDVIEW')) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}
$pgv_lang["accept_changes"]                      = "接受更改";
$pgv_lang["replace"]                = "更換記錄";
$pgv_lang["append"]                 = "添加記錄";
$pgv_lang["review_changes"]                      = "審核更改資料";
$pgv_lang["remove_object"]            = "移除資料";
$pgv_lang["remove_links"]            = "取消鏈接";
$pgv_lang["media_not_deleted"]        = "多媒體資料夾沒有被刪除。";
$pgv_lang["thumbs_not_deleted"]        = "縮圖資料夾沒有被刪除。";
$pgv_lang["thumbs_deleted"]            = "縮圖資料夾成功被刪除。";
$pgv_lang["show_thumbnail"]             = "顯示縮圖";
$pgv_lang["link_media"]             = "連接多媒體";
$pgv_lang["to_person"]                 = "至個人";
$pgv_lang["to_family"]                  = "至家庭";
$pgv_lang["to_source"]                  = "至信息源";
$pgv_lang["edit_fam"]                  = "編輯家庭";
$pgv_lang["copy"]                  = "複製";
$pgv_lang["cut"]                  = "裁切";
$pgv_lang["sort_by_birth"]               = "按出生日期排列";
$pgv_lang["reorder_children"]                         = "重新排列子女";
$pgv_lang["add_from_clipboard"]          = "加到剪貼板：";
$pgv_lang["record_copied"]              = "複製記錄";
$pgv_lang["add_unlinked_person"]                     = "加入一個沒有鏈接的人士";
$pgv_lang["add_unlinked_source"]                     = "加入一個沒有鏈接的來源";
$pgv_lang["server_file"]               = "在伺服器上的檔案名";
$pgv_lang["server_file_advice"]            = "不要更改原始檔案名";
$pgv_lang["server_file_advice2"]        = "請輸入一個開頭為 &laquo;http://&raquo; 的地址。";
$pgv_lang["add_asso"]                = "增加同事";
$pgv_lang["edit_sex"]                = "更改性別";
$pgv_lang["add_obje"]                = "增加多媒體檔";
$pgv_lang["add_name"]            = "增加名字";
$pgv_lang["edit_raw"]                = "編輯家譜程式設計原始檔案";
$pgv_lang["label_add_remote_link"]                     = "增加鏈接";
$pgv_lang["label_gedcom_id"]                                = "資料庫編碼";
$pgv_lang["label_local_id"]                                      = "個人編碼";
$pgv_lang["accept"]                  = "接受";
$pgv_lang["accept_all"]              = "全部接受";
$pgv_lang["accept_gedcom"]         = "打開更改，點擊開啟鏈結。接受所有更改為GEDCOM，再導入GEDCOM 檔。";
$pgv_lang["accept_successful"]        = "更改成功加入資料庫";
$pgv_lang["add_child"]            = "添加子女";
$pgv_lang["add_child_to_family"]    = "添加子女到這個家庭";
$pgv_lang["add_fact"]            = "添加情況";
$pgv_lang["add_father"]         = "添加父親";
$pgv_lang["add_husb"]            = "添加丈夫";
$pgv_lang["add_husb_to_family"]    = "在這家庭添加一位丈夫";
$pgv_lang["add_media"]        = "添加媒體文件";
$pgv_lang["add_media_lbl"]        = "添加媒體";
$pgv_lang["add_mother"]         = "添加母親";
$pgv_lang["add_new_chil"]                       = "添加子女";
$pgv_lang["add_new_husb"]        = "添加一位丈夫";
$pgv_lang["add_new_wife"]        = "添加一位妻子";
$pgv_lang["add_note"]                 = "添加來源情況附註";
$pgv_lang["add_note_lbl"]             = "添加附註";
$pgv_lang["add_sibling"]             = "添加兄弟姐妹";
$pgv_lang["add_son_daughter"]         = "添加子女";
$pgv_lang["add_source"]             = "添加來源情況";
$pgv_lang["add_source_lbl"]              = "添加資料源";
$pgv_lang["add_wife"]                 = "添加妻子";
$pgv_lang["add_wife_to_family"]         = "在這家庭添加一位妻子";
$pgv_lang["advanced_search_discription"] = "進級搜索";
$pgv_lang["auto_thumbnail"]              = "自動縮圖";
$pgv_lang["basic_search"]              = "基本搜索";
$pgv_lang["basic_search_discription"]          = "基本搜索描述";
$pgv_lang["birthdate_search"]              = "出生日期：";
$pgv_lang["birthplace_search"]                      = "出生地點：";
$pgv_lang["change"]                  = "更改";
$pgv_lang["change_family_instr"]          = "在此頁更改或刪除家庭成員。<br /><br /> 利用更改鏈接把某家庭成員替換為其他人，及以刪除鏈接把此人從家庭抽出。<br /><br />完成後，請按保存。<br />";
$pgv_lang["change_family_members"]     = "更改家庭成員";
$pgv_lang["changes_occurred"]         = "出現變更:";
$pgv_lang["create_repository"]         = "建立新檔";
$pgv_lang["create_source"]         = "建立新的來源紀錄";
$pgv_lang["current_person"]            = "保持個人現況";
$pgv_lang["date"]                  = "日期";
$pgv_lang["deathdate_search"]              = "去世日期：";
$pgv_lang["deathplace_search"]          = "去世地點：";
$pgv_lang["delete_dir_success"]          = "成功刪除媒體與縮圖";
$pgv_lang["delete_file"]        = "刪除檔案";
$pgv_lang["delete_repo"]        = "刪除資料庫";
$pgv_lang["directory_not_empty"]    = "資料夾不是空的";
$pgv_lang["directory_not_exist"]    = "資料夾不存在";
$pgv_lang["family"]            = "家庭";
$pgv_lang["file_missing"]        = "未發現檔案";
$pgv_lang["file_partial"]        = "檔案部份被上載";
$pgv_lang["file_success"]        = "檔案成功上載";
$pgv_lang["file_too_big"]        = "上載檔案超出允許的大小";
$pgv_lang["folder"]            = "伺服器上的資料夾";
$pgv_lang["gedcomid"]            = "GEDCOM 記錄身份證";
$pgv_lang["gedrec_deleted"]        = "GEDCOM 記錄被刪除";
$pgv_lang["gen_thumb"]        = "建立縮圖";
$pgv_lang["gender_search"]        = "性別：";
$pgv_lang["generate_thumbnail"]    = "自動建立縮圖";
$pgv_lang["hide_changes"]        = "隱藏更改紀錄";
$pgv_lang["highlighted"]        = "凸出的圖像";
$pgv_lang["illegal_chars"]        = "名字裡有不允許的字母或是空的";
$pgv_lang["label_password_id2"]    = "密碼：";
$pgv_lang["label_site"]                               = "網站";
$pgv_lang["label_username_id2"]     = "用戶名：";
$pgv_lang["media_file"]         = "媒體檔案";
$pgv_lang["must_provide"]         = "您必須提供 ";
$pgv_lang["name_search"]         = "搜索名字：";
$pgv_lang["no_temple"]         = "沒有寺廟。居住的法令";
$pgv_lang["photo_replace"]                      = "是否用此相片替換舊的一張？";
$pgv_lang["show_changes"]        = "記錄已被更改。點擊此處以顯示更新。";
$pgv_lang["thumb_genned"]        = "縮圖自動產生";
$pgv_lang["thumbgen_error"]        = "縮圖無法產生 ";
$pgv_lang["thumbnail"]            = "縮圖";
$pgv_lang["undo"]            = "取消";
$pgv_lang["undo_all"]            = "取消所有更改";
$pgv_lang["undo_all_confirm"]        = "確認取消所有更改？";
$pgv_lang["undo_successful"]        = "取消更改，成功復原。";
$pgv_lang["update_successful"]        = "更新成功";
$pgv_lang["upload"]            = "上載";
$pgv_lang["upload_error"]        = "上載錯誤。";
$pgv_lang["upload_media"]        = "上載多媒體檔";
$pgv_lang["upload_media_help"]    = "~#pgv_lang[upload_media]#~<br /><br />選擇上載檔案。所有檔案會上載到 <b>#MEDIA_DIRECTORY#</b> 或其子目錄。<br /><br />這些檔案名稱會出現在#MEDIA_DIRECTORY#。例如，#MEDIA_DIRECTORY#myfamily。 如製圖目錄不存在，將會自動創建。";
$pgv_lang["upload_successful"]        = "成功上載";
$pgv_lang["view_change_diff"]        = "瀏覽更改產生之差別";
$pgv_lang["add_marriage"]        = "增加婚姻";
$pgv_lang["edit_concurrency_change"] = "這紀錄上次被 <i>#CHANGEUSER#</i>在 #CHANGEDATE# 更改";
$pgv_lang["edit_concurrency_msg2"]    = "紀錄 #PID# 在你開啟後曾被其他人更改";
$pgv_lang["edit_concurrency_msg1"]    = "檔案建立時出現錯誤。可能在你開啟文件後不久曾被其他人更改。";
$pgv_lang["edit_concurrency_reload"]    = "請刷新你的瀏覽器以確保紀錄已被更新。";
$pgv_lang["admin_override"]        = "重寫管理專案";
$pgv_lang["no_update_CHAN"]        = "不更新修改事項";        
$pgv_lang["select_events"]            = "選擇活動";
$pgv_lang["source_events"]            = "連繫該活動到此資源";
$pgv_lang["advanced_name_fields"]      = "更多名字別稱 （如暱稱，婚後名字等。）";
?>