<?php
/**
 * Chinese texts
 *
 * phpGedView: Genealogy Viewer
 * Copyright (C) 2002 to 2008  PGV Development Team.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * @author PGV Developers
 * @package PhpGedView
 * @subpackage Languages
 * @version $Id: admin.zh.php 4194 2008-10-30 10:45:46Z fisharebest $
 * @translation mr_bobwang
 */

if (!defined('PGV_PHPGEDVIEW')) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

$pgv_lang["full_name"]            = "全名";
$pgv_lang["error_header"]         = "這個家譜文件, [#GEDCOM#], 內不存在你指定的標題。";
$pgv_lang["PRIV_PUBLIC"]         = "向公眾開放";
$pgv_lang["manage_gedcoms"]        = "管理家譜文件";
$pgv_lang["label_ban_server"]        = "發送";
$pgv_lang["label_delete"]               = "刪除";
$pgv_lang["upload_replacement"]    = "上載替換";
$pgv_lang["access"]            = "允許進入";
$pgv_lang["add_gedcom"]        = "添加其它GEDCOM文件";
$pgv_lang["add_new_gedcom"]     = "創建新的家譜GEDCOM檔";
$pgv_lang["add_new_language"]    = "加入一種新的語言";
$pgv_lang["add_user"]            = "加入新用戶";
$pgv_lang["admin_gedcom"]        = "管理某個家譜GEDCOM文件";
$pgv_lang["admin_gedcoms"]        = "管理多個家譜GEDCOM文件";
$pgv_lang["admin_geds"]        = "資料和家譜管理";
$pgv_lang["admin_site"]        = "網站管理";
$pgv_lang["admin_approved"]        = "您在服務器 #SERVER_NAME# 上的賬戶已被批准";
$pgv_lang["administration"]        = "管理";
$pgv_lang["ALLOW_CHANGE_GEDCOM"]= "可以更換其它家譜GEDCOM檔";
$pgv_lang["ALLOW_REMEMBER_ME"]   = "在登陸頁顯示 <b>記住我</b> ";
$pgv_lang["ALLOW_USER_THEMES"]    = "用戶可選擇自己的背景主題";
$pgv_lang["ansi_to_utf8"]        = "轉換這個ANSI (ISO-8859-1) 編碼GEDCOM 成UTF-8?";
$pgv_lang["apply_privacy"]        = "隱私設置";
$pgv_lang["back_useradmin"]        = "回到用戶管理";
$pgv_lang["bytes_read"]         = "閱讀Bytes:";
$pgv_lang["calc_marr_names"]        = "計算婚後的名字";
$pgv_lang["can_admin"]        = "用戶管理";
$pgv_lang["can_edit"]            = "可以編輯";
$pgv_lang["cleanup_places"]         = "清理地方";
$pgv_lang["cleanup_users"]        = "清理用戶";
$pgv_lang["click_here_to_continue"]    = "點擊此處繼續";
$pgv_lang["click_here_to_go_to_pedigree_tree"] = "點擊此處轉去家譜結構樹。";
$pgv_lang["config_help"]        = "設置幫助";
$pgv_lang["configuration"]        = "設置";
$pgv_lang["configure"]            = "設置家譜";
$pgv_lang["configure_head"]        = "家譜標題設置";
$pgv_lang["confirm_gedcom_delete"]    = "確認刪除這個家譜GEDCOM檔";
$pgv_lang["confirm_user_delete"]    = "確認刪除此用戶";
$pgv_lang["create_user"]        = "創建新用戶";
$pgv_lang["current_users"]        = "用戶列表";
$pgv_lang["daily"]            = "每天";
$pgv_lang["dataset_exists"]        = "以這個GEDCOM檔案名義導入的資料已經存在。";
$pgv_lang["date_registered"]        = "註冊日期";
$pgv_lang["day_before_month"]    = "日子要在月份前 (DD MM YYYY)";
$pgv_lang["DEFAULT_GEDCOM"]    = "設定的家譜GEDCOM 文件";
$pgv_lang["default_user"]        = "設定的用戶";
$pgv_lang["del_proceed"]        = "繼續";
$pgv_lang["del_unvera"]        = "用戶沒有被管理員確認";
$pgv_lang["del_unveru"]        = "用戶在七天內沒有確認";
$pgv_lang["do_not_change"]        = "不要更改";
$pgv_lang["download_gedcom"]    = "下載家譜GEDCOM文件";
$pgv_lang["download_here"]        = "下載此處";
$pgv_lang["editaccount"]        = "用戶可修改本人賬號資料";
$pgv_lang["duplicate_username"]    = "用戶名重複，請另選一個。";
$pgv_lang["empty_dataset"]        = "清空現有信息";
$pgv_lang["enable_disable_lang"]    = "啟用禁用的語種";
$pgv_lang["found_record"]        = "找到記錄";
$pgv_lang["ged_download"]        = "下載GEDCOM 文件";
$pgv_lang["ged_import"]        = "導入GEDCOM 文件";
$pgv_lang["ged_check"]         = "檢查GEDCOM 檔";
$pgv_lang["gedcom_adm_head"]    = "家譜GEDCOM 管理文件標題";
$pgv_lang["gedcom_file"]        = "家譜GEDCOM 文件:";
$pgv_lang["gedcom_not_imported"]    = "家譜GEDCOM檔還未被導入";
$pgv_lang["autoContinue"]        = "自動繼續";
$pgv_lang["import_complete"]        = "導入完成";
$pgv_lang["import_marr_names"]    = "輸入婚後名字";
$pgv_lang["import_options"]        = "導入選項";
$pgv_lang["import_progress"]        = "導入中...";
$pgv_lang["import_statistics"]        = "導入統計";
$pgv_lang["inc_languages"]        = "語言";
$pgv_lang["INDEX_DIRECTORY"]    = "工作資料夾";
$pgv_lang["label_families"]             = "家庭標記";
$pgv_lang["label_gedcom_id2"]           = "家譜標記代碼id2：";
$pgv_lang["label_individuals"]          = "個人標記";
$pgv_lang["label_password_id"]    = "密碼標記id";
$pgv_lang["label_username_id"]    = "用戶名標記id";
$pgv_lang["LANG_SELECTION"]         = "可選用的語言";
$pgv_lang["last_login"]            = "上次登入";
$pgv_lang["link_manage_servers"]       = "管理網站";
$pgv_lang["logfile_content"]        = "看電腦日記檔";
$pgv_lang["mailto"]            = "電郵地址";
$pgv_lang["merge_records"]        = "合併紀錄";
$pgv_lang["message_to_all"]        = "給所有使用者發短訊";
$pgv_lang["monthly"]            = "每月";
$pgv_lang["never"]            = "從不";
$pgv_lang["PGV_MEMORY_LIMIT"]    = "記憶體限制";
$pgv_lang["PGV_SESSION_SAVE_PATH"]= "工作資料夾";
$pgv_lang["PGV_SESSION_TIME"]    = "用戶時間";
$pgv_lang["phpinfo"]            = "PHP信息";
$pgv_lang["mysql"]            ="MySQL資料庫";
$pgv_lang["please_be_patient"]    = "請耐心等候";
$pgv_lang["privileges"]            = "特權";
$pgv_lang["reading_file"]        = "閱讀文件";
$pgv_lang["readme_documentation"]    = "讀我(README) 文件";
$pgv_lang["remove_ip"]         = "刪除IP地址";
$pgv_lang["REQUIRE_ADMIN_AUTH_REGISTRATION"]     = "需要管理人員確認新使用者之註冊";
$pgv_lang["rootid"]            = "起點人物代碼";
$pgv_lang["seconds"]            = "&nbsp;&nbsp;秒";
$pgv_lang["select_an_option"]        = "選項：";
$pgv_lang["SERVER_URL"]        = "家譜服務器網路位址";
$pgv_lang["show_phpinfo"]        = "顯示PHP資訊";
$pgv_lang["siteadmin"]            = "網頁管理員";
$pgv_lang["time_limit"]            = "時限：";
$pgv_lang["translator_tools"]        = "翻譯工具";
$pgv_lang["update_myaccount"]    = "更新本人賬號資料";
$pgv_lang["update_user"]        = "更新用戶";
$pgv_lang["upload_gedcom"]        = "上載的家譜GEDCOM文件";
$pgv_lang["USE_REGISTRATION_MODULE"]= "使用申請註冊模塊";
$pgv_lang["user_auto_accept"]              = "自動接受用戶的更改";
$pgv_lang["user_contact_method"]          = "用戶的聯繫方法";
$pgv_lang["user_create_error"]              = "建立新用戶失敗";
$pgv_lang["user_created"]              = "成功建立新用戶";
$pgv_lang["users_admin"]              = "用戶管理";
$pgv_lang["users_gedadmin"]              = "用戶的家譜GEDCOM文件管理";
$pgv_lang["users_total"]               = "用戶總數";
$pgv_lang["users_unver"]              = "沒有確認的用戶";
$pgv_lang["users_unver_admin"]        = "管理沒有被確認的用戶";
$pgv_lang["usr_deleted"]            = "刪除用戶：";
$pgv_lang["verified"]                          = "用戶自己確認User verified himself:";
$pgv_lang["verified_by_admin"]        = "用戶被管理員確認";
$pgv_lang["verify_gedcom"]            = "確認家譜GEDCOM文件";
$pgv_lang["visitor"]                = "訪客";
$pgv_lang["weekly"]                = "每週";
$pgv_lang["welcome_new"]            = "歡迎到訪我的家譜網站！";
$pgv_lang["yearly"]                = "每年";
$pgv_lang["admin_OK_subject"]        = "允許賬戶在 #SERVER_NAME#";
$pgv_lang["gedcheck"]                 = "家譜GEDCOM文件測驗";
$pgv_lang["level"]                        = "層次";                   
$pgv_lang["error"]                        = "出錯";
$pgv_lang["warning"]                  = "警告";
$pgv_lang["info"]                         = "信息";
$pgv_lang["all_rec"]                      = "所有記錄";
$pgv_lang["missing"]                  = "丟失";
$pgv_lang["data"]                         = "資料";
$pgv_lang["SURNAME_TRADITION"]                      = "姓氏在前是中文傳統";
$pgv_lang["you_may_login"]                       = "點擊下面鏈接以登錄本網絡平台:";
$pgv_lang["associated_files"]                        = "有聯繫的檔案：";
$pgv_lang["warn_file_delete"]                   = "此文件有重要資料，是否確定要刪除？";
$pgv_lang["deleted_files"]                  = "被刪除的檔案：";

$pgv_lang["index_dir_cleanup_inst"]    = "如想刪除此資料夾檔案，請把它們移到垃圾箱或在旁打鈎，再按下刪除便可永久刪掉。<br /><br />有<img src=\"./images/RESN_confidential.gif\" /> 標籤檔案不能被刪除，因仍被系統使用。<br /> 有<img src=\"./images/RESN_locked.gif\" /> 標籤檔案內有重要設置或等待變更的資料，故建議不要刪除。<br /><br />";
$pgv_lang["sanity_err0"]            = "錯誤：";
$pgv_lang["sanity_warn0"]            = "警告：";
$pgv_lang["refresh"]                = "刷新";
$pgv_lang["gedadmin"]                = "家譜管理員";
$pgv_lang["remove_all_files"]             = "刪除沒必要的檔案";
?>