<?php
/**
 * Chinese Language file for PhpGedView.
 *
 * phpGedView: Genealogy Viewer
 * Copyright (C) 2002 to 2008  PGV Development Team.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package PhpGedView
 * @version $Id: facts.zh.php 4194 2008-10-30 10:45:46Z fisharebest $
 * @translation mr_bobwang
 */

if (!defined('PGV_PHPGEDVIEW')) {
	header('HTTP/1.0 403 Forbidden');
	exit;
}
// -- Define a fact array to map GEDCOM tags with their chinese values
$factarray["ABBR"]                                 = "簡稱";
$factarray["ADDR"]                                 = "地址";
$factarray["ADR1"]                                  = "地址一";
$factarray["ADR2"]                                  = "地址二";
$factarray["ADOP"]                                  = "收養";
$factarray["AFN"]                                    = "祖先文件編號 (AFN)";
$factarray["AGE"]                                    = "年齡";
$factarray["AGNC"]                                 = "代辦處";
$factarray["ALIA"]                                    = "別名";
$factarray["ANCE"]                                  = "祖先";
$factarray["ANCI"]                                   = "祖先興趣";
$factarray["ANUL"]                                  = "取消";
$factarray["ASSO"]                                   = "同事";
$factarray["AUTH"]                                  = "作者";
$factarray["BAPL"]                                   = "LDS 洗禮";
$factarray["BAPM"]                                 = "洗禮";
$factarray["BARM"]                                = "Bar Mitzvah 猶太男孩割禮";
$factarray["BASM"]                                 = "Bas Mitzvah猶太女孩割禮";
$factarray["BIRT"]                                    = "生日";
$factarray["BLES"]                                    = "祝福";
$factarray["BLOB"]                                   = "數碼資料";
$factarray["BURI"]                                    = "埋葬";
$factarray["CALN"]                                   = "索書號";
$factarray["CAST"]                                    = "世襲的社會等級/社會狀態";
$factarray["CAUS"]                                   = "死因";
$factarray["CEME"]                                   = "墓地";
$factarray["CENS"]                                    = "人口調查";
$factarray["CHAN"]                                   = "前更改";
$factarray["CHAR"]                                    = "字元集";
$factarray["CHIL"]                                      = "子女";
$factarray["CHR"]                                       = "洗禮";
$factarray["CHRA"]                                     = "成人洗禮";
$factarray["CITY"]                                       = "城市";
$factarray["CONF"]                                     = "確認書";
$factarray["CONL"]                                     = "LDS 確認書";
$factarray["COPR"]                                     = "版權";
$factarray["CORP"]                                     = "公司";
$factarray["CREM"]                                     = "火葬";
$factarray["CTRY"]                                       = "國家";
$factarray["DATA"]                                      = "資料";
$factarray["DATE"]                                       = "日期";
$factarray["DEAT"]                                       = "去世";
$factarray["DESC"]                                       = "後代";
$factarray["DESI"]                                        = "後代利息";
$factarray["DEST"]                                       = "目的地";
$factarray["DIV"]                                         = "離婚";
$factarray["DIVF"]                                       = "離婚被歸檔";
$factarray["DSCR"]                                      = "說明";
$factarray["EDUC"]                                      = "教育";
$factarray["EMIG"]                                      = "移民";
$factarray["ENDL"]                                      = "LDS 捐贈";
$factarray["ENGA"]                                     = "訂婚";
$factarray["EVEN"]                                      = "活動";
$factarray["FAM"]                                        = "家庭";
$factarray["FAMC"]                                      = "家庭中的子女 ";
$factarray["FAMF"]                                      = "家庭文件";
$factarray["FAMS"]                                      = "家庭中的配偶";
$factarray["FCOM"]                                      = "第一份聖餐";
$factarray["FILE"]                                           = "外部文件:";
$factarray["FORM"]                                       = "格式";
$factarray["GIVN"]                                         = "名字：";
$factarray["GRAD"]                                        = "畢業";
$factarray["HUSB"]                                         = "丈夫";
$factarray["IDNO"]                                          = "確認號";
$factarray["IMMI"]                                          = "移民";
$factarray["LEGA"]                                          = "遺產承繼人";
$factarray["MARB"]                                        = "婚姻受阻";
$factarray["MARC"]                                        = "婚姻合約";
$factarray["MARL"]  = "結婚證書";
$factarray["MARR"]  = "婚姻";
$factarray["MEDI"]   = "媒體類型";
$factarray["MARS"]  = "婚姻結算";
$factarray["NAME"]  = "名字";
$factarray["NATI"]    = "國籍";
$factarray["NATU"]   = "入籍";
$factarray["NCHI"]    = "子女數目";
$factarray["NICK"]     = "暱稱";
$factarray["NMR"]     = "婚姻次數";
$factarray["NOTE"]    = "附註";
$factarray["NPFX"]     = "字首";
$factarray["NSFX"]     = "接尾辭";
$factarray["OBJE"]     = "多媒體";
$factarray["OCCU"]    = "職業";
$factarray["ORDI"]      = "法例";
$factarray["ORDN"]    = "整理";
$factarray["PAGE"]     = "引證詳細資料";
$factarray["PEDI"]       = "家譜";
$factarray["PLAC"]      = "地點";
$factarray["PHON"]    = "電話";
$factarray["POST"]     = "郵政編碼";
$factarray["PROB"]     = "遺囑認證";
$factarray["PROP"]     = "屬性";
$factarray["PUBL"]      = "發行";
$factarray["QUAY"]     = "資料可信性";
$factarray["RELA"]	= "關係";
$factarray["REPO"]         = "程式庫";
$factarray["REFN"]         = "參考編碼";
$factarray["RELI"]           = "宗教信仰";
$factarray["RESI"]           = "住宅";
$factarray["RESN"]         = "限制";
$factarray["RETI"]           = "退休";
$factarray["RFN"]           = "記錄文件編號";
$factarray["RIN"]            = "記錄身份證編號";
$factarray["ROLE"]         = "角色";
$factarray["SEX"]            = "性別";
$factarray["SLGC"]         = "LDS 兒童海豹捕獵";
$factarray["SLGS"]          = "LDS 配偶海豹捕獵";
$factarray["SOUR"]         = "來源";
$factarray["SPFX"]          = "姓氏稱謂";
$factarray["SSN"]            = "社會安全編號";
$factarray["STAE"]          = "州";
$factarray["STAT"]          = "狀態";
$factarray["SUBM"]        = "提交者";
$factarray["SUBN"]         = "提交";
$factarray["SURN"]         = "姓氏";
$factarray["TEMP"]         = "寺廟";
$factarray["TEXT"]          = "文本";
$factarray["WIFE"]          = "妻子";
$factarray["TIME"]          = "時間";
$factarray["TITL"]            = "銜頭";
$factarray["TYPE"]                         = "類型";
$factarray["WILL"]                         = "遺書";
$factarray["BURI:PLAC"]               = "埋葬地點";
$factarray["MARR:PLAC"]             = "結婚地點";
$factarray["DEAT:PLAC"]               = "去世地點";
$factarray["BIRT:PLAC"]                 = "出生地點";
$factarray["FAMC:HUSB:SURN"]  = "父親姓氏";
$factarray["_EMAIL"]                      = "電子郵件";
$factarray["EMAIL"]                         = "電子郵件";
$factarray["_TODO"]                       = "為完成的項目";
$factarray["_UID"]                           = "普遍標誌";
// These facts are specific to GEDCOM exports from Family Tree Maker
$factarray["_MDCL"]                       = "醫療";
$factarray["_DEG"]                          = "學位";
$factarray["_MILT"]                         = "兵役";
$factarray["_SEPR"]                         = "分居";
$factarray["_DETS"]                         = "一位配偶死亡";
$factarray["CITN"]                            = "公民身份";
$factarray["EMAL"]	                 = "電子郵件";
// Other common customized facts
$factarray["_ADPF"]                        = "由父親撫養";
$factarray["_ADPM"]                      = "由母親撫養";
$factarray["_AKAN"]                        = "暱稱";
$factarray["_AKA"] 	                  = "又被名為";
$factarray["_BRTM"]                        = "Brit mila";
$factarray["_COML"]                        = "普通法婚姻";
$factarray["_EYEC"]         = "眼睛顏色";
$factarray["_FNRL"]        = "葬禮";
$factarray["_HAIR"]         = "頭髮顏色";
$factarray["_HEIG"]         = "身高";
$factarray["_INTE"]          = "入暮";
$factarray["_MARI"]        = "婚姻目的";
$factarray["_MBON"]      = "婚姻集資";
$factarray["_MEDC"]        = "健康情況";
$factarray["_MILI"]           = "軍事";
$factarray["_NMR"]           = "未婚";
$factarray["_NLIV"]            = "已故";
$factarray["_NMAR"]         = "從未結婚";
$factarray["_PRMN"]         = "永久號碼";
$factarray["_WEIG"]           = "體重";
$factarray["_YART"]            = "Yartzeit";
$factarray["_MARNM"]      = "婚後名字";
$factarray["COMM"]     = "短紀錄";
$factarray["_BIRT_CHIL"]    = "子女出生";
$factarray["_MARR_CHIL"] = "子女結婚";
$factarray["_DEAT_CHIL"]   = "子女身故";
$factarray["_GEDF"]             = "家譜文件";
// GEDCOM 5.5.1 related facts
$factarray["FAX"]                 = "傳真";
$factarray["FACT"]               = "實況";
$factarray["WWW"]             = "網站";
$factarray["MAP"]             = "地圖";
$factarray["LATI"]              = "地球緯度";
$factarray["LONG"]           = "地球經度";
$factarray["FONE"]            = "發音";
$factarray["ROMN"]          = "羅馬化";

?>