

Customized  language files for phpGedView (PGV).  Only Chinese language files are customized here.

## Customization ##
To customize a particular file, use the corresponding `.en.php` as reference.

## INSTALL ##
Copy `languages` folder to the root of PGV install, override existing files. The Chinese language files  have the extention of `.zh.php`. 
